package com.administracion.lordoftherings.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.Spinner
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.administracion.lordoftherings.R
import com.administracion.lordoftherings.model.response.Response
import com.administracion.lordoftherings.view.callbak.OnRequestListener
import com.administracion.lordoftherings.view.callbak.OnResponseListener

class RedditListResultFragment(val listener: OnRequestListener): Fragment(),  AdapterView.OnItemSelectedListener, OnResponseListener {

    private lateinit var arrayString : Array<String>

    private lateinit var spnHobbitSelector : Spinner
    private lateinit var rvResponseList : RecyclerView

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.reddit_list_result_layout, container, false)

        arrayString = resources.getStringArray(R.array.sec_names)

        spnHobbitSelector = view.findViewById(R.id.spnHobbitSelector)
        rvResponseList = view.findViewById(R.id.rvResponseList)

        spnHobbitSelector.onItemSelectedListener = this
        rvResponseList.layoutManager = LinearLayoutManager(activity)
        rvResponseList.addItemDecoration(DividerItemDecoration(activity, DividerItemDecoration.HORIZONTAL))

        return view
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("Not yet implemented")
    }

    override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
        if (arrayString.get(p2).equals("")) {
            listener.onRequest(p0?.getItemAtPosition(p2).toString())
        }else{
            listener.onRequest(p0?.getItemAtPosition(p2).toString(), arrayString.get(p2))
        }
    }

    override fun onResponseReceived(response: Response) {
        rvResponseList.adapter = ResponseListAdapter(response.data.children)
    }

}