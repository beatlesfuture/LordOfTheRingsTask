package com.administracion.lordoftherings.view.callbak

import com.administracion.lordoftherings.model.response.Response

interface OnResponseListener {
    fun onResponseReceived(response: Response)
}