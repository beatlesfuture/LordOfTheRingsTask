package com.administracion.lordoftherings.view

import android.app.ProgressDialog
import android.os.Bundle
import android.widget.ProgressBar
import androidx.appcompat.app.AppCompatActivity
import com.administracion.lordoftherings.LordRingsMVP
import com.administracion.lordoftherings.R
import com.administracion.lordoftherings.model.response.Response
import com.administracion.lordoftherings.presenter.PresenterImpl
import com.administracion.lordoftherings.view.callbak.OnRequestListener
import com.administracion.lordoftherings.view.callbak.OnResponseListener
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.response_list_item.view.*

class MainActivity : AppCompatActivity(), LordRingsMVP.LordRingsView,
    OnRequestListener {

    private lateinit var presenter : LordRingsMVP.LordRingsPresenter
    private lateinit var responseToLisFarment : OnResponseListener
    private lateinit var responseToJsonResult : OnResponseListener
    private lateinit var progressBar : ProgressDialog

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        presenter = PresenterImpl(this)

        //cargando los fragmentos
        val redditListResultFragment = RedditListResultFragment(this)
        responseToLisFarment = redditListResultFragment

        val redditJsonResult = RedditJsonResult()
        responseToJsonResult = redditJsonResult;

        val adapter = ViewPagerAdapter(this.supportFragmentManager)
        //llenando el adaptador
        adapter.addFragment(redditListResultFragment, getString(R.string.result_fragment_title))
        adapter.addFragment(redditJsonResult, getString(R.string.json_fragment_title))

        viewPager.adapter = adapter

        tab.setupWithViewPager(viewPager)

        progressBar = ProgressDialog(this)
        progressBar.setMessage(getText(R.string.searchinng))
        progressBar.setCancelable(false)

    }

    override fun showResponseList(response: Response) {
        responseToLisFarment.onResponseReceived(response)
        responseToJsonResult.onResponseReceived(response)
        progressBar.dismiss()
    }

    override fun onRequest(toSearch: String) {
        progressBar.show()
        presenter.search(toSearch)
    }

    override fun onRequest(toSearch1: String, toSearch2: String) { progressBar.show()
       presenter.doubleSearch(toSearch1, toSearch2)
    }

}