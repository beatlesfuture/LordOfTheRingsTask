package com.administracion.lordoftherings.view.callbak

interface OnRequestListener {

    fun onRequest(toSearch: String)
    fun onRequest(toSearch1: String, toSearch2 : String)
}