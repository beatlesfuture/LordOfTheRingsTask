package com.administracion.lordoftherings.view

import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.administracion.lordoftherings.R
import com.administracion.lordoftherings.model.response.Child
import com.squareup.picasso.Picasso

class ResponseListAdapter (var responseList : List<Child>):

    RecyclerView.Adapter<ResponseListAdapter.ResponseListAdapterViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ResponseListAdapterViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.response_list_item, parent, false)
        return ResponseListAdapterViewHolder(v)
    }

    override fun getItemCount(): Int {
        return responseList.size
    }

    override fun onBindViewHolder(holder: ResponseListAdapterViewHolder, position: Int) {
        val child = responseList.get(position)
        holder.txtTitle.setText(child.data.title)
        Picasso.get().load(child.data.thumbnail)
            .into(holder.imgThumbnail);
    }

     class ResponseListAdapterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
         var txtTitle = itemView.findViewById<TextView>(R.id.txtTitle)
         var imgThumbnail = itemView.findViewById<ImageView>(R.id.imgThumbnail)
     }
}