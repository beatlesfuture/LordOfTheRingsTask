package com.administracion.lordoftherings.view

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.administracion.lordoftherings.R
import com.administracion.lordoftherings.model.response.Response
import com.administracion.lordoftherings.view.callbak.OnResponseListener
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.gson.Gson
import com.google.gson.GsonBuilder

class RedditJsonResult: Fragment(), OnResponseListener, View.OnClickListener{
    private lateinit var txtJsonResponse : TextView
    private lateinit var fabShare : FloatingActionButton

    private var jsonString : String = ""

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.reddit_json_response_layout, container, false)

        txtJsonResponse = view.findViewById(R.id.txtJsonResponse)
        fabShare = view.findViewById(R.id.fabShare)

        //fabShare.setOnClickListener(this)

        return view
    }

    override fun onResponseReceived(response: Response) {
        //obteniendo la respuesta en formato JSON
        val gson = GsonBuilder().setPrettyPrinting().create()
        jsonString = gson.toJson(response.data.children).toString()
        txtJsonResponse.setText(jsonString)
    }

    override fun onClick(p0: View?) {
        when(p0!!.id){
            R.id.fabShare ->{
                val sendIntent = Intent()
                sendIntent.action = Intent.ACTION_SEND
                sendIntent.type = "text/plain"
                sendIntent.putExtra(Intent.EXTRA_TEXT, jsonString)

                val shareIntent = Intent.createChooser(sendIntent, null)
                startActivity(shareIntent)
            }

        }
    }
}