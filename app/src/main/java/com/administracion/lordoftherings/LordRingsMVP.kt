package com.administracion.lordoftherings

import com.administracion.lordoftherings.model.response.Response

interface LordRingsMVP {

    interface LordRingsView{
        fun showResponseList(response : Response)
    }

    interface LordRingsPresenter{
        fun search(toSerach : String)
        fun doubleSearch(toSerach1: String, toSerach2: String)
        fun retriveResponse(response : Response)
    }

    interface LordRingsModel{
        fun search(toSerach : String)
    }
}