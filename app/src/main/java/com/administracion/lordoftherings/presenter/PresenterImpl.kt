package com.administracion.lordoftherings.presenter

import com.administracion.lordoftherings.LordRingsMVP
import com.administracion.lordoftherings.model.call.ModelImp
import com.administracion.lordoftherings.model.response.Response

class PresenterImpl(var view : LordRingsMVP.LordRingsView): LordRingsMVP.LordRingsPresenter {
    private val model : LordRingsMVP.LordRingsModel
    private var twoNNamesToSearch = false;
    private var responseList : ArrayList<Response> = arrayListOf()
    private var toSerach2 = ""

    init {
        model = ModelImp(this)
    }
    override fun search(toSerach: String) {
        val response : Response

        if (toSerach != null){
            model.search(toSerach)
        }
    }

    override fun retriveResponse(response: Response) {
        if (!twoNNamesToSearch) {
            view.showResponseList(response)
        }else{
            responseList.add(response)
            if (responseList.size > 1){
                responseList.get(0).data.children.addAll(responseList.get(1).data.children)
                twoNNamesToSearch = false
                view.showResponseList(responseList.get(0))
                responseList.clear()
            }else{
                search(toSerach2)
            }
        }
    }

    override fun doubleSearch(toSerach1: String, toSerach2: String) {
        twoNNamesToSearch = true
        search(toSerach1)
        this.toSerach2 = toSerach2
    }
}