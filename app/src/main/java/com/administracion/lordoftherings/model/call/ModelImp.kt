package com.administracion.lordoftherings.model.call

import com.administracion.lordoftherings.LordRingsMVP
import com.administracion.lordoftherings.model.response.Response

class ModelImp(val presenter : LordRingsMVP.LordRingsPresenter) : LordRingsMVP.LordRingsModel, OnServiceResponse {
    override fun search(toSerach: String) {
        CallLotrService(this).execute(toSerach)
    }

    override fun onResponse(response: Response) {
        presenter.retriveResponse(response)
    }
}