package com.administracion.lordoftherings.model.call

import android.os.AsyncTask
import com.administracion.lordoftherings.model.response.Response
import com.google.gson.GsonBuilder
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class CallLotrService(val listener: OnServiceResponse) : AsyncTask<String, Void, Response>() {
    private val BASE_URL = "https://www.reddit.com"
    private val LIMIT = "100"
    private lateinit var retrofit : Retrofit

    override fun onPreExecute() {
        super.onPreExecute()

        val builder = GsonBuilder()
        builder.excludeFieldsWithoutExposeAnnotation()

        retrofit = Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create(builder.create()))
            .build()
    }
    override fun doInBackground(vararg toSearchParams: String?): Response? {

        val call = retrofit.create(AppiService::class.java).search(toSearchParams[0]!!, "1", LIMIT).execute()

        return call.body()
    }

    override fun onPostExecute(result: Response?) {
        super.onPostExecute(result)
        listener.onResponse(result!!)
    }

}