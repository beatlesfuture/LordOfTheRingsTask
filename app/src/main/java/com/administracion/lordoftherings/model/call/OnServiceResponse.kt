package com.administracion.lordoftherings.model.call

import com.administracion.lordoftherings.model.response.Response

interface OnServiceResponse {
    fun onResponse(response: Response)
}