package com.administracion.lordoftherings.model.call

import com.administracion.lordoftherings.model.response.Response
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Headers
import retrofit2.http.Query

interface AppiService {
    @Headers("Authorization-key: jr-WFFSYabl6Yg")
    @GET("r/lotr/search/.json")
    fun search( @Query("q") toSearch : String,  @Query("restrict_sr") value : String, @Query("limit") limit : String) : Call<Response>
    // @Query("restrict_sr") value : String,
}